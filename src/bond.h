/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef LMP_BOND_H
#define LMP_BOND_H

#include <stdio.h>
#include "pointers.h"

namespace LAMMPS_NS {

class Bond : protected Pointers {
  friend class ThrOMP;
  friend class FixOMP;
 public:
  int allocated;
  int *setflag;
  int writedata;                  // 1 if writes coeffs to data file
  double energy;                  // accumulated energies
  double virial[6];               // accumulated virial
  double *eatom,**vatom;          // accumulated per-atom energy/virial

  int reinitflag;                // 1 if compatible with fix adapt and alike

  // KOKKOS host/device flag and data masks

  ExecutionSpace execution_space;
  unsigned int datamask_read,datamask_modify;
  int copymode;

  Bond(class LAMMPS *);
  virtual ~Bond();
  virtual void init();
  virtual void init_style() {}
  virtual void compute(int, int) = 0;
  virtual void settings(int, char **) {}
  virtual void coeff(int, char **) = 0;
  virtual double equilibrium_distance(int) = 0;
  virtual void write_restart(FILE *) = 0;
  virtual void read_restart(FILE *) = 0;
  virtual void write_data(FILE *) {}
  virtual double single(int, double, int, int, double &) = 0;
  virtual double memory_usage();
  virtual void *extract(char *, int &) {return NULL;}
  virtual void reinit();

  void write_file(int, char**);

 protected:
  int suffix_flag;             // suffix compatibility flag

  int evflag;
  int eflag_either,eflag_global,eflag_atom;
  int vflag_either,vflag_global,vflag_atom;
  int maxeatom,maxvatom;

  void ev_setup(int, int, int alloc = 1);
  void ev_tally(int, int, int, int, double, double, double, double, double);

  template <int NEWTON_BOND>
  inline void e_tally( int i, int j, int nlocal, double ebond, double &loc_energy ) {
    if (eflag_global) {
      if (NEWTON_BOND) loc_energy += ebond;
      else {
        double ebondhalf = 0.5*ebond;
        if (i < nlocal) loc_energy += ebondhalf;
        if (j < nlocal) loc_energy += ebondhalf;
      }
    }
    if (eflag_atom) {
      double ebondhalf = 0.5*ebond;
      if (NEWTON_BOND || i < nlocal) eatom[i] += ebondhalf;
      if (NEWTON_BOND || j < nlocal) eatom[j] += ebondhalf;
    }
  }

  template <int NEWTON_BOND>
  inline void v_tally( int i, int j, int nlocal, double fbond,
    double delx, double dely, double delz,  double *loc_virial ) {
    double v[6];
    v[0] = delx*delx*fbond;
    v[1] = dely*dely*fbond;
    v[2] = delz*delz*fbond;
    v[3] = delx*dely*fbond;
    v[4] = delx*delz*fbond;
    v[5] = dely*delz*fbond;

    if (vflag_global) {
      if (NEWTON_BOND) {
        loc_virial[0] += v[0];
        loc_virial[1] += v[1];
        loc_virial[2] += v[2];
        loc_virial[3] += v[3];
        loc_virial[4] += v[4];
        loc_virial[5] += v[5];
      } else {
        if (i < nlocal) {
          loc_virial[0] += 0.5*v[0];
          loc_virial[1] += 0.5*v[1];
          loc_virial[2] += 0.5*v[2];
          loc_virial[3] += 0.5*v[3];
          loc_virial[4] += 0.5*v[4];
          loc_virial[5] += 0.5*v[5];
        }
        if (j < nlocal) {
          loc_virial[0] += 0.5*v[0];
          loc_virial[1] += 0.5*v[1];
          loc_virial[2] += 0.5*v[2];
          loc_virial[3] += 0.5*v[3];
          loc_virial[4] += 0.5*v[4];
          loc_virial[5] += 0.5*v[5];
        }
      }
    }

    if (vflag_atom) {
      if (NEWTON_BOND || i < nlocal) {
        vatom[i][0] += 0.5*v[0];
        vatom[i][1] += 0.5*v[1];
        vatom[i][2] += 0.5*v[2];
        vatom[i][3] += 0.5*v[3];
        vatom[i][4] += 0.5*v[4];
        vatom[i][5] += 0.5*v[5];
      }
      if (NEWTON_BOND || j < nlocal) {
        vatom[j][0] += 0.5*v[0];
        vatom[j][1] += 0.5*v[1];
        vatom[j][2] += 0.5*v[2];
        vatom[j][3] += 0.5*v[3];
        vatom[j][4] += 0.5*v[4];
        vatom[j][5] += 0.5*v[5];
      }
    }
  }
};

}

#endif

/* ERROR/WARNING messages:

E: Bond coeffs are not set

No bond coefficients have been assigned in the data file or via the
bond_coeff command.

E: All bond coeffs are not set

All bond coefficients must be set in the data file or by the
bond_coeff command before running a simulation.

*/
