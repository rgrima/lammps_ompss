/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include "npair_half_bin_newton.h"
#include "neighbor.h"
#include "neigh_list.h"
#include "atom.h"
#include "atom_vec.h"
#include "molecule.h"
#include "domain.h"
#include "my_page.h"
#include "error.h"
#include "comm.h"

using namespace LAMMPS_NS;

/* ---------------------------------------------------------------------- */

NPairHalfBinNewton::NPairHalfBinNewton(LAMMPS *lmp) : NPair(lmp) {}

/* ----------------------------------------------------------------------
   binned neighbor list construction with full Newton's 3rd law
   each owned atom i checks its own bin and other bins in Newton stencil
   every pair stored exactly once by some processor
------------------------------------------------------------------------- */
#ifdef _OMPSS
void NPairHalfBinNewton::build(NeighList *list)
{
  int nlocal = atom->nlocal;
  int ntask = comm->nworkers;
  int st = nlocal/ntask;
  int mo = nlocal%ntask;
  int ini = 0;
  for (int ii=0; ii< ntask; ii++ ) {
    int size = mo > 0 ? st + !!(mo--) : st;
#   pragma omp task default(none) no_copy_deps firstprivate(ini,size,ii) \
    shared(atom,molecular,list,nlocal)
    {
      int          *type = atom->type;
      int          *mask = atom->mask;
      double         **x = atom->x;
      int      *molindex = atom->molindex;
      int       *molatom = atom->molatom;
      tagint        *tag = atom->tag;
      tagint   *molecule = atom->molecule;
      tagint   **special = atom->special;
      int     **nspecial = atom->nspecial;
      Molecule **onemols = atom->avec->onemols;
      int    moltemplate = molecular == 2? 1 : 0;
      int      *numneigh = list->numneigh;
      int   **firstneigh = list->firstneigh;
      int         *ilist = list->ilist;

      MyPage<int> *ipage = &(list->ipage[ii]);
      ipage->reset();
      for (int i = ini; i < ini+size; i++) {
        int *neighptr = ipage->vget();
        int    itype = type[i];
        double  xtmp = x[i][0];
        double  ytmp = x[i][1];
        double  ztmp = x[i][2];
        int        n = 0;

        int imol, iatom, tagprev;
        if (moltemplate) {
          imol = molindex[i];
          iatom = molatom[i];
          tagprev = tag[i] - iatom - 1;
        }

        // loop over rest of atoms in i's bin, ghosts are at end of linked list
        // if j is owned atom, store it, since j is beyond i in linked list
        // if j is ghost, only store if j coords are "above and to the right" of i
        for (int j = bins[i]; j >= 0; j = bins[j]) {
          if (j >= nlocal) {
            if (x[j][2] < ztmp) continue;
            if (x[j][2] == ztmp) {
              if (x[j][1] < ytmp) continue;
              if (x[j][1] == ytmp && x[j][0] < xtmp) continue;
            }
          }
          int jtype = type[j];
          if (exclude && exclusion( i, j, itype, jtype, mask, molecule ) ) continue;

          double delx = xtmp - x[j][0];
          double dely = ytmp - x[j][1];
          double delz = ztmp - x[j][2];
          double rsq = delx*delx + dely*dely + delz*delz;
          if (rsq <= cutneighsq[itype][jtype]) {
            if (molecular) {
              int which = 0;
              if (!moltemplate)
                which = find_special( special[i], nspecial[i], tag[j] );
              else if (imol >= 0)
                which = find_special( onemols[imol]->special[iatom],
                                      onemols[imol]->nspecial[iatom],
                                      tag[j]-tagprev );
              if (which == 0) neighptr[n++] = j;
              else if (domain->minimum_image_check(delx,dely,delz)) neighptr[n++] = j;
              else if (which > 0) neighptr[n++] = j ^ (which << SBBITS);
              // OLD: if (which >= 0) neighptr[n++] = j ^ (which << SBBITS);
            } else neighptr[n++] = j;
          }
        }
        // loop over all atoms in other bins in stencil, store every pair
        int ibin = atom2bin[i];
        for (int k = 0; k < nstencil; k++) {
          for (int j = binhead[ibin+stencil[k]]; j >= 0; j = bins[j]) {
            int jtype = type[j];
            if ( exclude && exclusion( i, j, itype, jtype, mask, molecule ) ) continue;

            double delx = xtmp - x[j][0];
            double dely = ytmp - x[j][1];
            double delz = ztmp - x[j][2];
            double rsq = delx*delx + dely*dely + delz*delz;
            if (rsq <= cutneighsq[itype][jtype]) {
              if (molecular) {
                int which = 0;
                if (!moltemplate)
                  which = find_special( special[i], nspecial[i], tag[j] );
                else if (imol >= 0)
                  which = find_special( onemols[imol]->special[iatom],
                                        onemols[imol]->nspecial[iatom],
                                        tag[j]-tagprev );
                if (which == 0) neighptr[n++] = j;
                else if (domain->minimum_image_check(delx,dely,delz)) neighptr[n++] = j;
                else if (which > 0) neighptr[n++] = j ^ (which << SBBITS);
                // OLD: if (which >= 0) neighptr[n++] = j ^ (which << SBBITS);
              } else neighptr[n++] = j;
            }
          }
        }
        numneigh[i] = n;
        ilist[i] = i;
        firstneigh[i] = neighptr;
        ipage->vgot(n);
        if (ipage->status())
          error->one(FLERR,"Neighbor list overflow, boost neigh_modify one");
      }
    }
    ini += size;
  }
  list->inum = nlocal;
# pragma omp taskwait
}
#else
void NPairHalfBinNewton::build(NeighList *list)
{
# pragma omp task
  {
  int i,j,k,n,itype,jtype,ibin,which,imol,iatom,moltemplate;
  tagint tagprev;
  double xtmp,ytmp,ztmp,delx,dely,delz,rsq;
  int *neighptr;

  double **x = atom->x;
  int *type = atom->type;
  int *mask = atom->mask;
  tagint *tag = atom->tag;
  tagint *molecule = atom->molecule;
  tagint **special = atom->special;
  int **nspecial = atom->nspecial;
  int nlocal = atom->nlocal;
  if (includegroup) nlocal = atom->nfirst;

  int *molindex = atom->molindex;
  int *molatom = atom->molatom;
  Molecule **onemols = atom->avec->onemols;
  if (molecular == 2) moltemplate = 1;
  else moltemplate = 0;

  int *ilist = list->ilist;
  int *numneigh = list->numneigh;
  int **firstneigh = list->firstneigh;
  MyPage<int> *ipage = list->ipage;

  printf( "%20.20s ****** ipage=%p   list=%p\n", __func__, ipage, list );
  ipage->status();
  ipage[0].status();
  ipage[1].status();
  MPI_Finalize(); exit(0);

  int inum = 0;
  ipage->reset();

  for (i = 0; i < nlocal; i++) {
    n = 0;
    printf( "*** GET INDEX=%d ", i );
    neighptr = ipage->vget();
    if (i==10) { MPI_Finalize(); exit(0); }

    itype = type[i];
    xtmp = x[i][0];
    ytmp = x[i][1];
    ztmp = x[i][2];
    if (moltemplate) {
      imol = molindex[i];
      iatom = molatom[i];
      tagprev = tag[i] - iatom - 1;
    }

    // loop over rest of atoms in i's bin, ghosts are at end of linked list
    // if j is owned atom, store it, since j is beyond i in linked list
    // if j is ghost, only store if j coords are "above and to the right" of i

    for (j = bins[i]; j >= 0; j = bins[j]) {
      if (j >= nlocal) {
        if (x[j][2] < ztmp) continue;
        if (x[j][2] == ztmp) {
          if (x[j][1] < ytmp) continue;
          if (x[j][1] == ytmp && x[j][0] < xtmp) continue;
        }
      }

      jtype = type[j];
      if (exclude && exclusion(i,j,itype,jtype,mask,molecule)) continue;

      delx = xtmp - x[j][0];
      dely = ytmp - x[j][1];
      delz = ztmp - x[j][2];
      rsq = delx*delx + dely*dely + delz*delz;

      if (rsq <= cutneighsq[itype][jtype]) {
        if (molecular) {
          if (!moltemplate)
            which = find_special(special[i],nspecial[i],tag[j]);
          else if (imol >= 0)
            which = find_special(onemols[imol]->special[iatom],
                                 onemols[imol]->nspecial[iatom],
                                 tag[j]-tagprev);
          else which = 0;
          if (which == 0) neighptr[n++] = j;
          else if (domain->minimum_image_check(delx,dely,delz))
            neighptr[n++] = j;
          else if (which > 0) neighptr[n++] = j ^ (which << SBBITS);
          // OLD: if (which >= 0) neighptr[n++] = j ^ (which << SBBITS);
        } else neighptr[n++] = j;
      }
    }

    // loop over all atoms in other bins in stencil, store every pair

    ibin = atom2bin[i];
    for (k = 0; k < nstencil; k++) {
      for (j = binhead[ibin+stencil[k]]; j >= 0; j = bins[j]) {
        jtype = type[j];
        if (exclude && exclusion(i,j,itype,jtype,mask,molecule)) continue;

        delx = xtmp - x[j][0];
        dely = ytmp - x[j][1];
        delz = ztmp - x[j][2];
        rsq = delx*delx + dely*dely + delz*delz;

        if (rsq <= cutneighsq[itype][jtype]) {
          if (molecular) {
            if (!moltemplate)
              which = find_special(special[i],nspecial[i],tag[j]);
            else if (imol >= 0)
              which = find_special(onemols[imol]->special[iatom],
                                   onemols[imol]->nspecial[iatom],
                                   tag[j]-tagprev);
            else which = 0;
            if (which == 0) neighptr[n++] = j;
            else if (domain->minimum_image_check(delx,dely,delz))
              neighptr[n++] = j;
            else if (which > 0) neighptr[n++] = j ^ (which << SBBITS);
            // OLD: if (which >= 0) neighptr[n++] = j ^ (which << SBBITS);
          } else neighptr[n++] = j;
        }
      }
    }

    ilist[inum++] = i;
    firstneigh[i] = neighptr;
    numneigh[i] = n;
    ipage->vgot(n);
    if (ipage->status())
      error->one(FLERR,"Neighbor list overflow, boost neigh_modify one");
  }

  list->inum = inum;
  }
# pragma omp taskwait
  MPI_Finalize(); exit(0);
}
#endif
