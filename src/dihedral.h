/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef LMP_DIHEDRAL_H
#define LMP_DIHEDRAL_H

#include <stdio.h>
#include "pointers.h"

namespace LAMMPS_NS {

class Dihedral : protected Pointers {
  friend class ThrOMP;
  friend class FixOMP;
 public:
  int allocated;
  int *setflag;
  int writedata;                     // 1 if writes coeffs to data file
  double energy;                     // accumulated energy
  double virial[6];                  // accumulated virial
  double *eatom,**vatom;             // accumulated per-atom energy/virial

  // KOKKOS host/device flag and data masks

  ExecutionSpace execution_space;
  unsigned int datamask_read,datamask_modify;
  int copymode;

  Dihedral(class LAMMPS *);
  virtual ~Dihedral();
  virtual void init();
  virtual void init_style() {}
  virtual void compute(int, int) = 0;
  virtual void settings(int, char **) {}
  virtual void coeff(int, char **) = 0;
  virtual void write_restart(FILE *) = 0;
  virtual void read_restart(FILE *) = 0;
  virtual void write_data(FILE *) {}
  virtual double memory_usage();

 protected:
  int suffix_flag;             // suffix compatibility flag

  int evflag;
  int eflag_either,eflag_global,eflag_atom;
  int vflag_either,vflag_global,vflag_atom;
  int maxeatom,maxvatom;

  void ev_setup(int, int, int alloc = 1);
  void ev_tally(int, int, int, int, int, int, double,
                double *, double *, double *, double, double, double,
                double, double, double, double, double, double);

  template <int NEWTON_BOND>
  inline void e_tally(int i1, int i2, int i3, int i4, int nlocal,
                        double edihedral, double &loc_energy )
  {
    if (eflag_global) {
      if (NEWTON_BOND) loc_energy += edihedral;
      else {
        double edihedralquarter = 0.25*edihedral;
        if (i1 < nlocal) loc_energy += edihedralquarter;
        if (i2 < nlocal) loc_energy += edihedralquarter;
        if (i3 < nlocal) loc_energy += edihedralquarter;
        if (i4 < nlocal) loc_energy += edihedralquarter;
      }
    }
    if (eflag_atom) {
      double edihedralquarter = 0.25*edihedral;
      if (NEWTON_BOND || i1 < nlocal) eatom[i1] += edihedralquarter;
      if (NEWTON_BOND || i2 < nlocal) eatom[i2] += edihedralquarter;
      if (NEWTON_BOND || i3 < nlocal) eatom[i3] += edihedralquarter;
      if (NEWTON_BOND || i4 < nlocal) eatom[i4] += edihedralquarter;
    }
  }


  template <int NEWTON_BOND>
  inline void v_tally( int i1, int i2, int i3, int i4, int nlocal,
                        double *f1, double *f3, double *f4,
                        double vb1x, double vb1y, double vb1z,
                        double vb2x, double vb2y, double vb2z,
                        double vb3x, double vb3y, double vb3z, double *loc_virial )
  {
    double v[6];

    v[0] = vb1x*f1[0] + vb2x*f3[0] + (vb3x+vb2x)*f4[0];
    v[1] = vb1y*f1[1] + vb2y*f3[1] + (vb3y+vb2y)*f4[1];
    v[2] = vb1z*f1[2] + vb2z*f3[2] + (vb3z+vb2z)*f4[2];
    v[3] = vb1x*f1[1] + vb2x*f3[1] + (vb3x+vb2x)*f4[1];
    v[4] = vb1x*f1[2] + vb2x*f3[2] + (vb3x+vb2x)*f4[2];
    v[5] = vb1y*f1[2] + vb2y*f3[2] + (vb3y+vb2y)*f4[2];

    if (vflag_global) {         
      if (NEWTON_BOND) {        
        loc_virial[0] += v[0];      
        loc_virial[1] += v[1];      
        loc_virial[2] += v[2];      
        loc_virial[3] += v[3];      
        loc_virial[4] += v[4];      
        loc_virial[5] += v[5];      
      } else {                  
        if (i1 < nlocal) {      
          loc_virial[0] += 0.25*v[0];
          loc_virial[1] += 0.25*v[1];
          loc_virial[2] += 0.25*v[2];
          loc_virial[3] += 0.25*v[3];
          loc_virial[4] += 0.25*v[4];
          loc_virial[5] += 0.25*v[5];
        }                     
        if (i2 < nlocal) {    
          loc_virial[0] += 0.25*v[0];
          loc_virial[1] += 0.25*v[1];
          loc_virial[2] += 0.25*v[2];
          loc_virial[3] += 0.25*v[3];
          loc_virial[4] += 0.25*v[4];
          loc_virial[5] += 0.25*v[5];
        }                     
        if (i3 < nlocal) {    
          loc_virial[0] += 0.25*v[0];
          loc_virial[1] += 0.25*v[1];
          loc_virial[2] += 0.25*v[2];
          loc_virial[3] += 0.25*v[3];
          loc_virial[4] += 0.25*v[4];
          loc_virial[5] += 0.25*v[5];
        }                     
        if (i4 < nlocal) {    
          loc_virial[0] += 0.25*v[0];
          loc_virial[1] += 0.25*v[1];
          loc_virial[2] += 0.25*v[2];
          loc_virial[3] += 0.25*v[3];
          loc_virial[4] += 0.25*v[4];
          loc_virial[5] += 0.25*v[5];
        }                     
      }                       
    }                         

    if (vflag_atom) {         
      if (NEWTON_BOND || i1 < nlocal) {
        vatom[i1][0] += 0.25*v[0];
        vatom[i1][1] += 0.25*v[1];
        vatom[i1][2] += 0.25*v[2];
        vatom[i1][3] += 0.25*v[3];
        vatom[i1][4] += 0.25*v[4];
        vatom[i1][5] += 0.25*v[5];
      }                       
      if (NEWTON_BOND || i2 < nlocal) {
        vatom[i2][0] += 0.25*v[0];
        vatom[i2][1] += 0.25*v[1];
        vatom[i2][2] += 0.25*v[2];
        vatom[i2][3] += 0.25*v[3];
        vatom[i2][4] += 0.25*v[4];
        vatom[i2][5] += 0.25*v[5];
      }                       
      if (NEWTON_BOND || i3 < nlocal) {
        vatom[i3][0] += 0.25*v[0];
        vatom[i3][1] += 0.25*v[1];
        vatom[i3][2] += 0.25*v[2];
        vatom[i3][3] += 0.25*v[3];
        vatom[i3][4] += 0.25*v[4];
        vatom[i3][5] += 0.25*v[5];
      }                       
      if (NEWTON_BOND || i4 < nlocal) {
        vatom[i4][0] += 0.25*v[0];
        vatom[i4][1] += 0.25*v[1];
        vatom[i4][2] += 0.25*v[2];
        vatom[i4][3] += 0.25*v[3];
        vatom[i4][4] += 0.25*v[4];
        vatom[i4][5] += 0.25*v[5];
      }                         
    }                           
  }                             
};

}

#endif

/* ERROR/WARNING messages:

E: Dihedral coeffs are not set

No dihedral coefficients have been assigned in the data file or via
the dihedral_coeff command.

E: All dihedral coeffs are not set

All dihedral coefficients must be set in the data file or by the
dihedral_coeff command before running a simulation.

*/
