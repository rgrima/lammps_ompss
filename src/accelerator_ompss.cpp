/* ----------------------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#include <string.h>
#include <stdio.h>
#include <stdlib.h>
#include "accelerator_ompss.h"
#include "atom.h"
#include "neigh_list.h"
#include "npair.h"
#include "nbin.h"
#include "comm.h"
#include "domain.h"
#include "neighbor.h"
#include "force.h"
#include "pair.h"

using namespace LAMMPS_NS;

OmpssLMP::OmpssLMP( LAMMPS *lmp ) : Pointers(lmp) {
  pneight     = NULL;
  ntask       = -1;
  nneight     = 8;
  t_natoms    = NULL;
  t_list      = NULL;
  initialized = 0;
  centinel    = NULL;
  v_cent      = NULL;
  //
  t_nbondlist = NULL;
  t_bondlist  = NULL;
  t_nanglelist = NULL;
  t_anglelist  = NULL;
  t_ndihedrallist = NULL;
  t_dihedrallist  = NULL;
  t_nimproperlist = NULL;
  t_improperlist  = NULL;
}

OmpssLMP::~OmpssLMP( ) {
  if ( pneight ) {
    delete [] pneight[0];
    delete [] pneight;
  }

  if ( t_natoms ) delete [] t_natoms;
  if (t_list) {
    for (int i=0; i< ntask; i++)
      if (t_list[i] ) delete [] t_list[i];
    delete [] t_list;
  }

  if (t_nbondlist) {
    for (int i=0; i< ntask; i++)
      if (t_bondlist[i] ) delete [] t_bondlist[i];
    delete [] t_bondlist;
    delete [] t_nbondlist;
  }
  if (t_nanglelist) {
    for (int i=0; i< ntask; i++)
      if (t_anglelist[i] ) delete [] t_anglelist[i];
    delete [] t_anglelist;
    delete [] t_nanglelist;
  }
  if (t_ndihedrallist) {
    for (int i=0; i< ntask; i++)
      if (t_dihedrallist[i] ) delete [] t_dihedrallist[i];
    delete [] t_dihedrallist;
    delete [] t_ndihedrallist;
  }
  if (t_nimproperlist) {
    for (int i=0; i< ntask; i++)
      if (t_improperlist[i] ) delete [] t_improperlist[i];
    delete [] t_improperlist;
    delete [] t_nimproperlist;
  }
}

void OmpssLMP::init( ) {
  initialized = 1;
  NBin *nb = force->pair->list->np->nb;
  mbinlo[0] = nb->mbinxlo;
  mbinlo[1] = nb->mbinylo;
  mbinlo[2] = nb->mbinzlo;
  // Get the limits of the bin grid
  double sublo[3], subhi[3];
  if (domain->triclinic)
    domain->bbox(domain->sublo_lamda,domain->subhi_lamda,sublo,subhi);
  else {
    sublo[0] = domain->sublo[0];
    sublo[1] = domain->sublo[1];
    sublo[2] = domain->sublo[2];
    subhi[0] = domain->subhi[0];
    subhi[1] = domain->subhi[1];
    subhi[2] = domain->subhi[2];
  }
  bininv[0] = nb->bininvx;
  bininv[1] = nb->bininvy;
  bininv[2] = nb->bininvz;
  double *bboxlo = neighbor->bboxlo;
  double *bboxhi = neighbor->bboxhi;
  for (int i=0; i< 3; i++) {
    bin_first[i] = (static_cast<int>(floor(sublo[i]-bboxlo[i])*bininv[i]))-mbinlo[i];
    bin_last[i]  = (static_cast<int>(ceil(subhi[i]-bboxlo[i])*bininv[i]))-mbinlo[i];
    grsize[i] = bin_last[i]-bin_first[i]+1;
  }
  computeThrgrid( );
  if (!comm->me) printf( "%d by %d by %d OMPSS thread grid\n", thrgrid[0], thrgrid[1], thrgrid[2] );

  int n_inc = 0;
  for (int i=0; i< 3; i++)
    if (thrgrid[i] != 1) thr_nTa[i] = n_inc++ < 2 ? 3 : 2;
    else thr_nTa[i] = 1;

  for (int i=0; i< 3; i++) taskGrid[i] = thr_nTa[i]*thrgrid[i];
  ntask = taskGrid[0]*taskGrid[1]*taskGrid[2];

  nneight = 8;
  if (! pneight  ) {
    pneight  = new int*[ntask];
    int *tmp = new int[ntask*8];
    for (int i=0; i< ntask; i++, tmp += 8) pneight[i] = tmp;
  }
  if (! t_natoms ) t_natoms = new int[ntask];
  if (! t_list   ) {
    t_list   = new int*[ntask];
    for (int i=0; i< ntask; i++) t_list[i] = NULL;
  }
  int scen = 1;
  for (int i=0; i< 3; i++) if (taskGrid[0]>1) scen *= (taskGrid[0]+1);
  if (! centinel ) centinel = new int[scen];
  if (! v_cent) v_cent = new int[ntask];

  if (atom->molecular)
  {
    if (force->bond) {
      if (! t_nbondlist) t_nbondlist = new int[ntask];
      if (! t_bondlist) {
        t_bondlist = new int**[ntask];
        for (int i=0; i< ntask; i++) t_bondlist[i] = NULL;
      }
    }
    if (force->angle) {
      if (! t_nanglelist) t_nanglelist = new int[ntask];
      if (! t_anglelist) {
        t_anglelist = new int**[ntask];
        for (int i=0; i< ntask; i++) t_anglelist[i] = NULL;
      }
    }
    if (force->dihedral) {
      if (! t_ndihedrallist) t_ndihedrallist = new int[ntask];
      if (! t_dihedrallist) {
        t_dihedrallist = new int**[ntask];
        for (int i=0; i< ntask; i++) t_dihedrallist[i] = NULL;
      }
    }
    if (force->improper) {
      if (! t_nimproperlist) t_nimproperlist = new int[ntask];
      if (! t_improperlist) {
        t_improperlist = new int**[ntask];
        for (int i=0; i< ntask; i++) t_improperlist[i] = NULL;
      }
    }
  }
}

void OmpssLMP::build( ) {
  if (!initialized) init( );
  int t=0;
  for (int i=0; i< taskGrid[2]; i++)
    for (int j=0; j< taskGrid[1]; j++)
      for (int k=0; k< taskGrid[0]; k++, t++) {
        int id = k + j*(taskGrid[0]+1) + i*(taskGrid[0]+1)*(taskGrid[1]+1);
        pneight[t][0] = id;
        pneight[t][1] = id+1;
        pneight[t][2] = pneight[t][1]+taskGrid[0];
        pneight[t][3] = pneight[t][2]+1;
        pneight[t][4] = pneight[t][0]+(taskGrid[0]+1)*(taskGrid[1]+1);
        pneight[t][5] = pneight[t][4]+1;
        pneight[t][6] = pneight[t][5]+taskGrid[0];
        pneight[t][7] = pneight[t][6]+1;
      }
  for (int i=0; i< 3; i++) {
    div_bins[i] = (grsize[i]-1)/taskGrid[i];
    mod_bins[i] = grsize[i]-1 - div_bins[i]*taskGrid[i];
  }
  NBin *nb = force->pair->list->np->nb;
  int *binhead = nb->binhead;
  for ( t=0; t< ntask; t++) {
    int coord[3], ini[3], end[3];
    from1Dto3D( t, taskGrid, coord );
    for ( int i=0; i< 3; i++ ) {
      ini[i] = bin_first[i] + coord[i]*div_bins[i] + MIN(coord[i],mod_bins[i]);
      end[i] = bin_first[i] + (coord[i]+1)*div_bins[i] + MIN(coord[i]+1,mod_bins[i]);
      if (coord[i]==taskGrid[i]-1) end[i]++;
    }
    int n = 0;
    for (int i=ini[2]; i< end[2]; i++)
      for (int j=ini[1]; j< end[1]; j++)
        for (int k=ini[0]; k< end[0]; k++) {
          int b = k + j*nb->mbinx + i*nb->mbinx*nb->mbiny;
          int id = binhead[b];
          while ( id >= 0 ) {
            if ( id < atom->nlocal ) n++;
            id  = nb->bins[id];
          }
        }
    t_natoms[t] = n;
    if (t_list[t]) delete [ ] t_list[t];
    t_list[t] = new int[n];
    n = 0;
    for (int i=ini[2]; i< end[2]; i++)
      for (int j=ini[1]; j< end[1]; j++)
        for (int k=ini[0]; k< end[0]; k++) {
          int b = k + j*nb->mbinx + i*nb->mbinx*nb->mbiny;
          int id = binhead[b];
          while ( id >= 0 ) {
            if ( id < atom->nlocal ) {
              t_list[t][n] = id;
              n++;
            }
            id  = nb->bins[id];
          }
        }
  }
  if (atom->molecular)
  {
    if (force->bond) buildBondlist( );
    if (force->angle) buildAnglelist( ); 
    if (force->dihedral) buildDihedrallist( ); 
    if (force->improper) buildImproperlist( );
  }
}

void OmpssLMP::buildBondlist( ) {
  int nbondlist = neighbor->nbondlist;
  int **bondlist = neighbor->bondlist;
  int nlocal = atom->nlocal;
  memset( t_nbondlist, 0, ntask*sizeof(int) );
  for (int i=0; i< nbondlist; i++) {
    int a = bondlist[i][0] < nlocal ? bondlist[i][0] : bondlist[i][1];
    int t = atom2Task( a );
    t_nbondlist[t]++;
  }
  for ( int t=0; t< ntask; t++)
  {
    if (t_bondlist[t]) delete [] t_bondlist[t];
    t_bondlist[t] = t_nbondlist[t] ? new int*[t_nbondlist[t]] : NULL;
  }
  memset( t_nbondlist, 0, ntask*sizeof(int) );
  for (int i=0; i< nbondlist; i++) {
    int a = bondlist[i][0] < nlocal ? bondlist[i][0] : bondlist[i][1];
    int t = atom2Task( a );
    t_bondlist[t][t_nbondlist[t]++] = bondlist[i];
  }
}

void OmpssLMP::buildAnglelist( ) {
  int nanglelist = neighbor->nanglelist;
  int **anglelist = neighbor->anglelist;
  int nlocal = atom->nlocal;

  memset( t_nanglelist, 0, ntask*sizeof(int) );
  for (int i=0; i< nanglelist; i++) {
    int a = anglelist[i][0] < nlocal ? anglelist[i][0] :
          ( anglelist[i][1] < nlocal ? anglelist[i][1] : anglelist[i][2] );
    int t = atom2Task( a );
    t_nanglelist[t]++;
  }
  for ( int t=0; t< ntask; t++) {
    if (t_anglelist[t]) delete [] t_anglelist[t];
    t_anglelist[t] = t_nanglelist[t] ? new int*[t_nanglelist[t]] : NULL;
  }
  memset( t_nanglelist, 0, ntask*sizeof(int) );
  for (int i=0; i< nanglelist; i++) {
    int a = anglelist[i][0] < nlocal ? anglelist[i][0] :
          ( anglelist[i][1] < nlocal ? anglelist[i][1] : anglelist[i][2] );
    int t = atom2Task( a );
    t_anglelist[t][t_nanglelist[t]++] = anglelist[i];
  }
}

void OmpssLMP::buildDihedrallist( ) {
  int ndihedrallist = neighbor->ndihedrallist;
  int **dihedrallist = neighbor->dihedrallist;
  int nlocal = atom->nlocal;

  memset( t_ndihedrallist, 0, ntask*sizeof(int) );
  for (int i=0; i< ndihedrallist; i++) {
    int a = dihedrallist[i][0] < nlocal ? dihedrallist[i][0] :
          ( dihedrallist[i][1] < nlocal ? dihedrallist[i][1] :
          ( dihedrallist[i][2] < nlocal ? dihedrallist[i][2] : dihedrallist[i][3] ) );
    int t = atom2Task( a );
    t_ndihedrallist[t]++;
  }
  for ( int t=0; t< ntask; t++) {
    if (t_dihedrallist[t]) delete [] t_dihedrallist[t];
    t_dihedrallist[t] = t_ndihedrallist[t] ? new int*[t_ndihedrallist[t]] : NULL;
  }
  memset( t_ndihedrallist, 0, ntask*sizeof(int) );
  for (int i=0; i< ndihedrallist; i++) {
    int a = dihedrallist[i][0] < nlocal ? dihedrallist[i][0] :
          ( dihedrallist[i][1] < nlocal ? dihedrallist[i][1] :
          ( dihedrallist[i][2] < nlocal ? dihedrallist[i][2] : dihedrallist[i][3] ) );
    int t = atom2Task( a );
    t_dihedrallist[t][t_ndihedrallist[t]++] = dihedrallist[i];
  }
}

void OmpssLMP::buildImproperlist( ) {
  int nimproperlist = neighbor->nimproperlist;
  int **improperlist = neighbor->improperlist;
  int nlocal = atom->nlocal;

  memset( t_nimproperlist, 0, ntask*sizeof(int) );
  for (int i=0; i< nimproperlist; i++) {
    int a = improperlist[i][0] < nlocal ? improperlist[i][0] :
          ( improperlist[i][1] < nlocal ? improperlist[i][1] :
          ( improperlist[i][2] < nlocal ? improperlist[i][2] : improperlist[i][3] ) );
    int t = atom2Task( a );
    t_nimproperlist[t]++;
  }
  for ( int t=0; t< ntask; t++) {
    if (t_improperlist[t]) delete [] t_improperlist[t];
    t_improperlist[t] = t_nimproperlist[t] ? new int*[t_nimproperlist[t]] : NULL;
  }
  memset( t_nimproperlist, 0, ntask*sizeof(int) );
  for (int i=0; i< nimproperlist; i++) {
    int a = improperlist[i][0] < nlocal ? improperlist[i][0] :
          ( improperlist[i][1] < nlocal ? improperlist[i][1] :
          ( improperlist[i][2] < nlocal ? improperlist[i][2] : improperlist[i][3] ) );
    int t = atom2Task( a );
    t_improperlist[t][t_nimproperlist[t]++] = improperlist[i];
  }
}

int OmpssLMP::GetTaskId( int t ) {
  int ta_id = t/comm->nworkers;
  int th_id = t - ta_id*comm->nworkers;

  int coord[3], lcoord[3], gcoord[3];
  from1Dto3D( th_id, thrgrid, coord );
  from1Dto3D( ta_id, thr_nTa, lcoord );
  for (int i=0; i< 3; i++) gcoord[i] = coord[i]*thr_nTa[i]+lcoord[i];
  return gcoord[2]*taskGrid[1]*taskGrid[0] + gcoord[1]*taskGrid[0] + gcoord[0];
};

void OmpssLMP::computeThrgrid( )
{
  int npossible = 0;
  for (int i = 1; i <= comm->nworkers; i++) {
    if (comm->nworkers % i) continue;
    int nyz = comm->nworkers/i;
    for (int j = 1; j <= nyz; j++) {
      if (nyz % j) continue;
      npossible++;
    }
  }
  int **factors = new int*[npossible];
  int m = 0;
  for (int i = 1; i <= comm->nworkers; i++) {
    if (comm->nworkers % i) continue;
    int nyz = comm->nworkers/i;
    for (int j = 1; j <= nyz; j++) {
      if (nyz % j) continue;
      factors[m] = new int[3];
      factors[m][0] = i;
      factors[m][1] = j;
      factors[m][2] = nyz/j;
      m++;
    }
  }
  double area[3] = { grsize[0]*grsize[1], grsize[0]*grsize[2], grsize[1]*grsize[2] };
  double surf;
  double bestsurf = 2.0 * (area[0]+area[1]+area[2]);
  for (int i = 0; i < npossible; i++) {
    surf = area[0]/factors[i][0]/factors[i][1] +
           area[1]/factors[i][0]/factors[i][2] +
           area[2]/factors[i][1]/factors[i][2];
    if (surf < bestsurf) {
      bestsurf = surf;
      thrgrid[0] = factors[i][0];
      thrgrid[1] = factors[i][1];
      thrgrid[2] = factors[i][2];
    }
    delete [] factors[i];
  }
  delete [] factors;
}

void OmpssLMP::print( )
{
  char fn[128];
  sprintf( fn, "graph_mesh_%03d.gv", comm->me );
  FILE *fd = fopen(fn,"w");
  fprintf( fd, "digraph graph_mesh {\n" );
  for (int i=0; i< ntask; i++)
    for (int j=0; j< nneight; j++)
      fprintf( fd, "  node_%05d -> node_%05d;\n", i, pneight[i][j] );
  fprintf( fd, "}\n" );
  fclose( fd );
}

inline void OmpssLMP::from1Dto3D( int i, int *X, int *vec ) {
  vec[0] = i%X[0];
  vec[1] = (i/X[0])%X[1];
  vec[2] = i/(X[0]*X[1]);
}

inline int OmpssLMP::atom2Task( int a ) {
  double *bboxlo = neighbor->bboxlo;
  double *bboxhi = neighbor->bboxhi;
  int task[3];
  for (int i=0; i< 3; i++) {
    int bin = (static_cast<int>((atom->x[a][i]-bboxlo[i])*bininv[i])) - mbinlo[i]- bin_first[i];
    if  ( bin>=(grsize[i]-1) ) task[i] = taskGrid[i]-1;
    else if (mod_bins[i]==0) task[i] = bin/div_bins[i];
    else {
      task[i]= bin/(div_bins[i]+1);
      if (task[i]>=mod_bins[i]) task[i] = (bin-mod_bins[i])/div_bins[i];
    }
  }
  return task[0] + task[1]*taskGrid[0] + task[2]*taskGrid[0]*taskGrid[1];
}
