/* -*- c++ -*- ----------------------------------------------------------
   LAMMPS - Large-scale Atomic/Molecular Massively Parallel Simulator
   http://lammps.sandia.gov, Sandia National Laboratories
   Steve Plimpton, sjplimp@sandia.gov

   Copyright (2003) Sandia Corporation.  Under the terms of Contract
   DE-AC04-94AL85000 with Sandia Corporation, the U.S. Government retains
   certain rights in this software.  This software is distributed under
   the GNU General Public License.

   See the README file in the top-level LAMMPS directory.
------------------------------------------------------------------------- */

#ifndef LMP_ANGLE_H
#define LMP_ANGLE_H

#include <stdio.h>
#include "pointers.h"

namespace LAMMPS_NS {

class Angle : protected Pointers {
  friend class ThrOMP;
  friend class FixOMP;
 public:
  int allocated;
  int *setflag;
  int writedata;                  // 1 if writes coeffs to data file
  double energy;                  // accumulated energies
  double virial[6];               // accumulated virial
  double *eatom,**vatom;          // accumulated per-atom energy/virial

  // KOKKOS host/device flag and data masks

  ExecutionSpace execution_space;
  unsigned int datamask_read,datamask_modify;
  int copymode;

  Angle(class LAMMPS *);
  virtual ~Angle();
  virtual void init();
  virtual void compute(int, int) = 0;
  virtual void settings(int, char **) {}
  virtual void coeff(int, char **) = 0;
  virtual void init_style() {};
  virtual double equilibrium_angle(int) = 0;
  virtual void write_restart(FILE *) = 0;
  virtual void read_restart(FILE *) = 0;
  virtual void write_data(FILE *) {}
  virtual double single(int, int, int, int) = 0;
  virtual double memory_usage();

 protected:
  int suffix_flag;             // suffix compatibility flag

  int evflag;
  int eflag_either,eflag_global,eflag_atom;
  int vflag_either,vflag_global,vflag_atom;
  int maxeatom,maxvatom;

  void ev_setup(int, int, int alloc = 1);
  void ev_tally(int, int, int, int, int, double, double *, double *,
                double, double, double, double, double, double);

  template <int NEWTON_BOND>
  inline void e_tally( int i, int j, int k, int nlocal,
                       double eangle, double &loc_energy ) {
    static const double THIRD  = 1.0/3.0;
    if (eflag_global) {
      if (NEWTON_BOND) loc_energy += eangle;
      else {
        double eanglethird = THIRD*eangle;
        if (i < nlocal) loc_energy += eanglethird;
        if (j < nlocal) loc_energy += eanglethird;
        if (k < nlocal) loc_energy += eanglethird;
      }
    }
    if (eflag_atom) {
      double eanglethird = THIRD*eangle;
      if (NEWTON_BOND || i < nlocal) eatom[i] += eanglethird;
      if (NEWTON_BOND || j < nlocal) eatom[j] += eanglethird;
      if (NEWTON_BOND || k < nlocal) eatom[k] += eanglethird;
    }
  }

  template <int NEWTON_BOND>
  inline void v_tally(int i, int j, int k, int nlocal, double *f1, double *f3,
                     double delx1, double dely1, double delz1,
                     double delx2, double dely2, double delz2, double *loc_virial )
  {
    static const double THIRD  = 1.0/3.0;
    double v[6];
    v[0] = delx1*f1[0] + delx2*f3[0];
    v[1] = dely1*f1[1] + dely2*f3[1];
    v[2] = delz1*f1[2] + delz2*f3[2];
    v[3] = delx1*f1[1] + delx2*f3[1];
    v[4] = delx1*f1[2] + delx2*f3[2];
    v[5] = dely1*f1[2] + dely2*f3[2];

    if (vflag_global) {
      if (NEWTON_BOND) {
        loc_virial[0] += v[0];
        loc_virial[1] += v[1];
        loc_virial[2] += v[2];
        loc_virial[3] += v[3];
        loc_virial[4] += v[4];
        loc_virial[5] += v[5];
      } else {
        if (i < nlocal) {
          loc_virial[0] += THIRD*v[0];
          loc_virial[1] += THIRD*v[1];
          loc_virial[2] += THIRD*v[2];
          loc_virial[3] += THIRD*v[3];
          loc_virial[4] += THIRD*v[4];
          loc_virial[5] += THIRD*v[5];
        }
        if (j < nlocal) {
          loc_virial[0] += THIRD*v[0];
          loc_virial[1] += THIRD*v[1];
          loc_virial[2] += THIRD*v[2];
          loc_virial[3] += THIRD*v[3];
          loc_virial[4] += THIRD*v[4];
          loc_virial[5] += THIRD*v[5];
        }
        if (k < nlocal) {
          loc_virial[0] += THIRD*v[0];
          loc_virial[1] += THIRD*v[1];
          loc_virial[2] += THIRD*v[2];
          loc_virial[3] += THIRD*v[3];
          loc_virial[4] += THIRD*v[4];
          loc_virial[5] += THIRD*v[5];
        }
      }
    }
    if (vflag_atom) {
      if (NEWTON_BOND || i < nlocal) {
        vatom[i][0] += THIRD*v[0];
        vatom[i][1] += THIRD*v[1];
        vatom[i][2] += THIRD*v[2];
        vatom[i][3] += THIRD*v[3];
        vatom[i][4] += THIRD*v[4];
        vatom[i][5] += THIRD*v[5];
      }
      if (NEWTON_BOND || j < nlocal) {
        vatom[j][0] += THIRD*v[0];
        vatom[j][1] += THIRD*v[1];
        vatom[j][2] += THIRD*v[2];
        vatom[j][3] += THIRD*v[3];
        vatom[j][4] += THIRD*v[4];
        vatom[j][5] += THIRD*v[5];
      }
      if (NEWTON_BOND || k < nlocal) {
        vatom[k][0] += THIRD*v[0];
        vatom[k][1] += THIRD*v[1];
        vatom[k][2] += THIRD*v[2];
        vatom[k][3] += THIRD*v[3];
        vatom[k][4] += THIRD*v[4];
        vatom[k][5] += THIRD*v[5];
      }
    }
  }
};

}

#endif

/* ERROR/WARNING messages:

E: Angle coeffs are not set

No angle coefficients have been assigned in the data file or via the
angle_coeff command.

E: All angle coeffs are not set

All angle coefficients must be set in the data file or by the
angle_coeff command before running a simulation.

*/
