#!/bin/bash

lammps=$(realpath ../../lmp_mpi)
inputf=$(realpath in_200.lj)
extraef=$(realpath extrae_ompss.xml)

NCPUS=48
for threads in 16 24 48; do
    for OMPSS_CHUNK_SIZE in 8192 10240 12288 14336 16384 ; do
        for OMPSS_CHUNK_BSIZE in 32 40 48 56 64 72 80; do
            procs=$(( ${NCPUS}/${threads} ))
            dir=$( printf "tchunk-Pr%02dTr%02d-%05d-%03d" "${procs}" "${threads}" "${OMPSS_CHUNK_SIZE}" "${OMPSS_CHUNK_BSIZE}" )
            [ -d ${dir} ] && rm -rf ${dir}; mkdir ${dir}
            pushd ${dir} &> /dev/null
cat << EOF > run.sh
#!/bin/bash

ulimit -s 16384

export OMP_NUM_THREADS=\${SLURM_CPUS_PER_TASK}

export EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/openmpi_1_10_7/
export EXTRAE_CONFIG_FILE=${extraef}
export LD_PRELOAD=\$EXTRAE_HOME/lib/libnanosmpitrace.so

export NX_INSTRUMENTATION=extrae
export OMPSS_CHUNK_SIZE=${OMPSS_CHUNK_SIZE}
export OMPSS_CHUNK_BSIZE=${OMPSS_CHUNK_BSIZE}
${lammps} -i ${inputf}

EOF
            chmod +x run.sh
cat << EOF > lammps.sh
#!/bin/bash
#SBATCH --job-name=LAMMPS
#SBATCH --workdir=.
#SBATCH --output=LAMMPS.out
#SBATCH --error=LAMMPS.err
#SBATCH --ntasks=${procs}
#SBATCH --cpus-per-task=${threads}
#SBATCH --time=00:30:00
#SBATCH --qos=debug

LIST_MOD=\$( module tablelist 2>&1 | grep \" | cut -d\" -f 2 )

echo \${LIST_MOD} | grep -w intel > /dev/null && module swap intel gcc/7.1.0
echo \${LIST_MOD} | grep -w impi  > /dev/null && module swap impi openmpi
echo \${LIST_MOD} | grep -w ompss > /dev/null || module load ompss

srun ./run.sh &
pid=\$!
[ -f memory_info ] && rm memory_info
while ps -p \${pid} > /dev/null; do
  sleep 1
  cat /proc/meminfo | grep Active:  | awk '{print \$2}' >> memory_info
done

EOF
            sbatch lammps.sh
            popd &> /dev/null
        done
    done
done
