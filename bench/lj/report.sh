#!/bin/bash
TAG=tchunk
echo "========  OMPSS TIME ========"
echo "         MIN    MAX"
for f in ${TAG}*/*out; do
#  echo $( echo ${f} | awk -F '[-/]' '{print $2}' ) $( grep -w Pair $f | awk -F '[|]' '{print $2 $4}')
  echo $( echo ${f} | awk -F '[-/]' '{print $2 " " $3 " " $4}' ) $( grep -w Pair $f | awk -F '[|]' '{print $2 $4}')
done
echo ""
echo "========  OMPSS MEMORY ========"

for f in ${TAG}*/mem*; do
  echo $( echo ${f} | awk -F '[-/]' '{print $2}' ) $( sort -g $f | tail -n 1 )
done
