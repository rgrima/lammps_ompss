#!/bin/bash

lammps=$(realpath ../../lmp_mpi)
inputf=$(realpath in_200.lj)

for procs in 48 24 16 12 8 6 4 3 2
do
    threads=$(( 48/${procs} ))
    dir=$( printf "memory-Pr%02dTr%02d" "${procs}" "${threads}" )
    [ -d ${dir} ] && rm -rf ${dir}; mkdir ${dir}
    pushd ${dir} &> /dev/null
cat << EOF > run.sh
#!/bin/bash
export OMP_NUM_THREADS=\${SLURM_CPUS_PER_TASK}

[ \${OMP_NUM_THREADS} -gt 1 ] && EXTRA_FLAGS="-sf omp -pk omp \${OMP_NUM_THREADS}"

${lammps} \${EXTRA_FLAGS} -i ${inputf}
EOF
    chmod +x run.sh
cat << EOF > lammps.sh
#!/bin/bash
#SBATCH --job-name=LAMMPS
#SBATCH --workdir=.
#SBATCH --output=LAMMPS.out
#SBATCH --error=LAMMPS.err
#SBATCH --ntasks=${procs}
#SBATCH --cpus-per-task=${threads}
#SBATCH --time=00:30:00
#SBATCH --qos=debug

module swap intel intel/2018.3
module swap impi impi/2018.3

srun ./run.sh &
pid=\$!
[ -f memory_info ] && rm memory_info
while ps -p \${pid} > /dev/null; do
  sleep 1
  cat /proc/meminfo | grep Active:  | awk '{print \$2}' >> memory_info
done

EOF
    sbatch lammps.sh
    popd &> /dev/null
done
