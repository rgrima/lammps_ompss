#!/bin/bash

MSG=$( nm -a ../../lmp_mpi )
for a in main _ZN9LAMMPS_NS6LAMMPSC2EiPPci _ZN9LAMMPS_NS6LAMMPS6createEv _ZN9LAMMPS_NS5Input4fileEv \
_ZN9LAMMPS_NS11CreateAtoms7commandEiPPc _ZN9LAMMPS_NS5Input7latticeEv _ZN9LAMMPS_NS9CreateBox7commandEiPPc \
_ZN9LAMMPS_NS8Velocity7commandEiPPc _ZN9LAMMPS_NS6Modify4initEv _ZN9LAMMPS_NS8Neighbor4initEv \
_ZN9LAMMPS_NS6LAMMPS4initEv _ZN9LAMMPS_NS6Verlet5setupEi _ZN9LAMMPS_NS6Domain3pbcEv \
_ZN9LAMMPS_NS5Force5setupEv _ZN9LAMMPS_NS6Verlet3runEi _ZN9LAMMPS_NS9Integrate6ev_setEl \
_ZN9LAMMPS_NS6Modify23initial_integrate_respaEiii _ZN9LAMMPS_NS6Modify14post_integrateEv \
_ZN9LAMMPS_NS9CommBrick12forward_commEi _ZN9LAMMPS_NS9PairLJCut7computeEii _ZN9LAMMPS_NS12PairLJCutOMP7computeEii \
_ZN9LAMMPS_NS6Modify11pre_reverseEii _ZN9LAMMPS_NS6Modify12pre_exchangeEv _ZN9LAMMPS_NS6Modify12pre_neighborEv \
_ZN9LAMMPS_NS6Modify13post_neighborEv _ZN9LAMMPS_NS6Modify9pre_forceEi _ZN9LAMMPS_NS6Modify10post_forceEi \
_ZN9LAMMPS_NS9CommBrick8exchangeEv _ZN9LAMMPS_NS9CommBrick7bordersEv _ZN9LAMMPS_NS9CommBrick12reverse_commEv
do
  msg=$( echo "$MSG" |  grep " T $a" )
  echo ${msg/" T "/"#"}
  echo ${msg/" T "/"#"}+outerloops
done > functions.dat

xml=$(realpath extrae.xml)
lammps=$(realpath ../../lmp_mpi)
inputf=$(realpath in_200.lj)

for procs in 48 24 16 8 4 2
do
    threads=$(( 48/${procs} ))
    dir=$( printf "traza-Pr%02dTr%02d" "${procs}" "${threads}" )
    [ -d ${dir} ] && rm -rf ${dir}; mkdir ${dir}
    cp functions.dat ${dir}
    pushd ${dir} &> /dev/null
cat << EOF > run.sh
#!/bin/bash
export OMP_NUM_THREADS=\${SLURM_CPUS_PER_TASK}
export EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2018_1
export EXTRAE_CONFIG_FILE=${xml}
export LD_PRELOAD=\${EXTRAE_HOME}/lib/libompitrace.so

[ \${OMP_NUM_THREADS} -gt 1 ] && EXTRA_FLAGS="-sf omp -pk omp \${OMP_NUM_THREADS}"

${lammps} \${EXTRA_FLAGS} -i ${inputf}
EOF
    chmod +x run.sh
cat << EOF > lammps.sh
#!/bin/bash
#SBATCH --job-name=LAMMPS
#SBATCH --workdir=.
#SBATCH --output=LAMMPS.out
#SBATCH --error=LAMMPS.err
#SBATCH --ntasks=${procs}
#SBATCH --cpus-per-task=${threads}
#SBATCH --time=00:30:00
#SBATCH --qos=debug

module swap intel intel/2018.3
module swap impi impi/2018.3

srun ./run.sh
EOF
    sbatch lammps.sh
    popd &> /dev/null
done
