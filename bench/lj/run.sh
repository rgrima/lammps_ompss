#!/bin/bash
TRACE=NO
INFILE=in.lj
NPROCS=4

[ ! -z "$BSC_MACHINE" ] && {
    LISTMOD=$(echo $LOADEDMODULES | tr ":" "\n")
    MOD=$( echo "${LISTMOD}" | grep -w ompss ) || module load ompss
}

[ "${TRACE}" == "YES" ] && { make -C ../../ extrae || exit; }
[ "${TRACE}" == "NO"  ] && { make -C ../../ basic || exit; }


[ $# -ge 1 ] && [ ${1} -eq ${1} ] && [ ${1} -gt 0 ] && NPROCS=${1}
{ [ ${NPROCS} -eq 1 ] && MPIRUN=; } || MPIRUN="mpirun -np ${NPROCS}"

[ $# -ge 1 ] && [ ${1} -eq ${1} ] && [ ${1} -gt 0 ] && NPROCS=${1}

SC="/tmp/lammps.sh"
cat << EOF > ${SC}
#!/bin/bash
export OMP_NUM_THREADS=1
[ $# -ge 2 ] && [ ${2} -eq ${2} ] && [ ${2} -gt 0 ] && export OMP_NUM_THREADS=${2}
if [ "${TRACE}" == "YES" ]; then
  export EXTRAE_CONFIG_FILE=extrae_ompss.xml
  [ -d /opt/extrae-3.6.1 ] && export EXTRAE_HOME=/opt/extrae-3.6.1
  [ -d /apps/BSCTOOLS/extrae/latest/impi_2017_4 ] && export EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4
  export NX_INSTRUMENTATION=extrae
  export NX_ARGS="--instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state"
  [ ${NPROCS} -gt 1 ] && export LD_PRELOAD=\${EXTRAE_HOME}/lib/libnanosmpitrace.so
  EXEC=../../lmp_ompss_ext
else
  EXEC=../../lmp_ompss

fi
stdbuf -oL \${EXEC} -i ${INFILE}
EOF
chmod +x ${SC}
${MPIRUN} ${SC}
