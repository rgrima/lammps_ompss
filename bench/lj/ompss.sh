#!/bin/bash

TRACE=NO

[ ! -z "$BSC_MACHINE" ] && {
    LISTMOD=$(echo $LOADEDMODULES | tr ":" "\n")
    MOD=$( echo "${LISTMOD}" | grep -w ompss ) || module load ompss
}

pushd ../..
[ "${TRACE}" == "YES" ] && { make extrae || exit; }
[ "${TRACE}" == "NO" ] && { make basic  || exit; }
popd

NNODES=1
NCPUS=$(( 48*${NNODES} ))
SIZE=1024
BSIZE=32
TEST_DIR=OMPSS

lammps=$(realpath ../../lmp_ompss)$( [ "${TRACE}" == "YES" ] && echo _ext)
inputf=$(realpath in_200.lj)
extraef=$(realpath extrae_ompss.xml)
export EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4

if [ -d ${TEST_DIR} ]; then
  i=1
  tt=$( printf "${TEST_DIR}-%02d" "${i}" )
  while [ -d ${tt} ]; do i=$(( i+1 )); tt=$( printf "${TEST_DIR}-%02d" "${i}" ); done
  mv ${TEST_DIR} ${tt}
fi
mkdir ${TEST_DIR}; cd ${TEST_DIR}

cat << EOF > lammps.sh
#!/bin/bash
#SBATCH --job-name=LAMMPS
#SBATCH --workdir=.
#SBATCH --output=LAMMPS.out
#SBATCH --error=LAMMPS.err
#SBATCH --ntasks=${NCPUS}
#SBATCH --cpus-per-task=1
#SBATCH --time=00:30:00
#SBATCH --qos=debug

LISTMOD=\$(echo \$LOADEDMODULES | tr ":" "\n")
MOD=\$( echo "\${LISTMOD}" | grep -w gcc ) && module swap \${MOD} intel
MOD=\$( echo "\${LISTMOD}" | grep -w openmpi ) && module swap \${MOD} impi
MOD=\$( echo "\${LISTMOD}" | grep -w ompss ) || module load ompss

ulimit -s 16384

#for threads in 1 2 4 8 16 24 48; do
for threads in 1 8 16 24 48; do
    procs=\$(( ${NCPUS}/\${threads} ))
    export SLURM_CPUS_PER_TASK=\${threads}
    export SLURM_NPROCS=\${procs}
    export SLURM_NTASKS=\${procs}
    export SLURM_TASKS_PER_NODE="\$(( \${procs}/${NNODES} ))(x${NNODES})"
    dir=\$( printf "Pr%02dTr%02d" "\${procs}" "\${threads}" )
    [ -d \${dir} ] && rm -rf \${dir}; mkdir \${dir}
    pushd \${dir} &> /dev/null

cat << EOOF > run.sh
#!/bin/bash

export OMP_NUM_THREADS=\${threads}

export PATH=${EXTRAE_HOME}/bin/:\${PATH}
if [ "${TRACE}" == "YES" ]; then
    export EXTRAE_CONFIG_FILE=${extraef}
    export NX_INSTRUMENTATION=extrae
    export NX_ARGS="--instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state"
    export LD_PRELOAD=${EXTRAE_HOME}/lib/libnanosmpitrace.so
fi
#ulimit -c unlimited
ulimit -s 16384
stdbuf -oL ${lammps} -i ${inputf}
EOOF
    chmod +x run.sh
    stdbuf -oL srun ./run.sh > stdout 2> stderr
    ls *.prv &> /dev/null || ${EXTRAE_HOME}/bin/mpi2prv -f TRACE.mpits
    popd
done

EOF
sbatch lammps.sh
