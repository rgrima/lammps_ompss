#!/bin/bash
#SBATCH --job-name=LAMMPS
#SBATCH --workdir=.
#SBATCH --output=LAMMPS_%j.out
#SBATCH --error=LAMMPS_%j.err
#SBATCH --ntasks=1
#SBATCH --cpus-per-task=48
#SBATCH --tasks-per-node=1
#SBATCH --time=00:20:00
#SBATCH --qos=debug

# Choose the execution MODE: aps, trace, basic, amplxe
MODE=amplxe

# get total number of CPUs
NCPUS=$(( ${SLURM_CPUS_PER_TASK}*${SLURM_NPROCS} ))

# Create a Host file from SLURM variables
hfile=$(realpath .hfile.${SLURM_JOB_ID})
scontrol show hostnames $SLURM_JOB_NODELIST > ${hfile}

# Load proper Intel versions
[ "${INTEL_VERSION}" != "2018.3.051" ] && module swap intel intel/2018.3        &> /dev/null
[ "${MPI_V}" != "2018.3.051" ] && module swap impi impi/2018.3                  &> /dev/null

# Load needed Intel variables according to the MODE
[ "${MODE}" == "aps" ]    && source ${INTEL_HOME}/vtune_amplifier/apsvars.sh     &> /dev/null
[ "${MODE}" == "trace" ]  && source ${INTEL_HOME}/itac/*/intel64/bin/itacvars.sh &> /dev/null
[ "${MODE}" == "amplxe" ] && source ${INTEL_HOME}/vtune_amplifier/amplxe-vars.sh &> /dev/null

# Define our executable. Use realpath in order to work under workdirs
EXEC="$( realpath ../../lmp_mpi) -i $( realpath in_200.lj )"

for nt in 1 4 8 16 48
do
    export OMP_NUM_THREADS=$nt
    export NP=$(( NCPUS/nt ))
    export PPN=$(( 48/nt ))
    WDIR=$( printf "%s_Pr%02d_Th%02d" $MODE $NP $OMP_NUM_THREADS )
    [ -d ${WDIR} ] && rm -rf ${WDIR}
    mkdir ${WDIR} &> /dev/null
    pushd ${WDIR}
    MPI_EXEC="mpirun -n ${NP} -ppn ${PPN} -f ${hfile}"
    EXEC_EXTRA="-sf omp -pk omp ${OMP_NUM_THREADS}"
    [ "${MODE}" == "aps" ]    && { DIR=aps-report; MPI_EXTRA="aps -r ${DIR}"; }
    [ "${MODE}" == "trace" ]  && { MPI_EXTRA="-trace"; export VT_LOGFILE_FORMAT=SINGLESTF; }
    [ "${MODE}" == "amplxe" ] && { TARGET_PID=0; MPI_EXTRA=( -gtool "amplxe-cl -collect hpc-performance -data-limit=0 -r result_init:${TARGET_PID}" ); }
     echo ${MPI_EXEC} "${MPI_EXTRA[@]}" ${EXEC} ${EXEC_EXTRA}
    ( time ${MPI_EXEC} "${MPI_EXTRA[@]}" ${EXEC} ${EXEC_EXTRA} ) &> result.txt
    [ "${MODE}" == "aps" ] && aps-report ${DIR} -O ${DIR}.html
    popd
done

rm ${hfile}
