#!/bin/bash
TRACE=NO
INFILE=in.rhodo.scaled
NPROCS=4
NTHREADS=1
EXTRAE_FILE=extrae_ompss.xml

[ $# -ge 1 ] && [ ${1} -eq ${1} ] && [ ${1} -gt 0 ] && NPROCS=${1}
{ [ ${NPROCS} -eq 1 ] && MPIRUN=; } || MPIRUN="mpirun --map-by socket:OVERSUBSCRIBE -np ${NPROCS}"

[ $# -ge 2 ] && [ ${2} -eq ${2} ] && [ ${2} -gt 0 ] && export NTHREADS=${2}

if [ "${TRACE}" == "YES" ]; then
    make -C ../../ extrae || exit
    EXEC=../../lmp_ompss_ext
else
    make -C ../../ basic || exit
    EXEC=../../lmp_ompss
fi

EXTRAE_HOME=
[ -d /opt/extrae-3.6.1 ] && export EXTRAE_HOME=/opt/extrae-3.6.1
[ -d /apps/BSCTOOLS/extrae/latest/impi_2017_4 ] && export EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4

SC="/tmp/lammps.sh"
cat << EOF > ${SC}
#!/bin/bash
if [ "${TRACE}" == "YES" ]; then
  export EXTRAE_CONFIG_FILE=${EXTRAE_FILE}
  export NX_INSTRUMENTATION=extrae
  export NX_ARGS="--instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state"
  [ ${NPROCS} -gt 1 ] && export LD_PRELOAD=${EXTRAE_HOME}/lib/libnanosmpitrace.so
fi
export OMP_NUM_THREADS=${NTHREADS}
stdbuf -oL ${EXEC} -i ${INFILE}
EOF
chmod +x ${SC}
${MPIRUN} ${SC}
