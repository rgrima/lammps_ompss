#!/bin/bash

input_f="in.rhodo.scaled"
data_f="data.rhodo extrae.xml"
topdir=$(realpath ../)
original_files=$( for f in ${input_f} ${data_f}; do realpath $f; done )

EXTRAE_HOME=/apps/BSCTOOLS/extrae/latest/impi_2017_4

GNU=NO
TEST_DIR=TRACES
#rm -rf ${TEST_DIR} &> /dev/null

if [ -d ${TEST_DIR} ]; then
  i=1
  tt=$( printf "${TEST_DIR}-%02d" "${i}" )
  while [ -d ${tt} ]; do i=$(( i+1 )); tt=$( printf "${TEST_DIR}-%02d" "${i}" ); done
  mv ${TEST_DIR} ${tt}
fi
mkdir ${TEST_DIR}; cd ${TEST_DIR}

NNODES=1
NCPUS=$(( NNODES*48 ))

cat << EOF > lammps.sh
#!/bin/bash
#SBATCH --job-name=LAMMPS
#SBATCH --workdir=.
#SBATCH --output=LAMMPS.out
#SBATCH --error=LAMMPS.err
#SBATCH --ntasks=$(( 48*${NNODES} ))
#SBATCH --cpus-per-task=1
#SBATCH --time=00:60:00
#SBATCH --qos=debug

OMP="${topdir}/lmp_omp"
OMPSS="${topdir}/lmp_ompss_ext"
KOKKOS="${topdir}/lmp_kokkos"
for threads in 6; do
    procs=\$(( ${NCPUS} / \${threads} ))
    export SLURM_CPUS_PER_TASK=\${threads}
    export SLURM_NPROCS=\${procs}
    export SLURM_NTASKS=\${procs}
    export SLURM_TASKS_PER_NODE="\$(( \${procs}/${NNODES} ))(x${NNODES})"
    for MOD in "OMPSS" "OMP" "KOKKOS"; do
        fn=\$( printf "\${MOD}-%04d-%02d" "\${procs}" "\${threads}" )
        mkdir \${fn}
        pushd \${fn} &> /dev/null
        cp $( echo ${original_files} ) .
        echo -e "%s/x index 1/x index 2/g\n" \
                "%s/y index 1/y index 3/g\n" \
                "%s/z index 1/z index 3/g\nw\nq\n" | ex ${input_f}
        script=\${fn}.sh
        EXEC=\${!MOD}
        EXTRA_FLAGS=""
        if [ \${threads} -gt 1 ]; then
            [ \${MOD} == "OMP" ]    && EXTRA_FLAGS="-sf omp -pk omp \${threads}"
            [ \${MOD} == "KOKKOS" ] && EXTRA_FLAGS="-k on t \${threads} -sf kk" && export OMP_PROC_BIND=true
        fi
cat << EOOF > \${script}
#!/bin/bash
export OMP_NUM_THREADS=\${threads}
export EXTRAE_CONFIG_FILE=extrae.xml
if [ "\${MOD}" == "OMPSS" ]; then
    export NX_INSTRUMENTATION=extrae
    export NX_ARGS="--instrument-default=user --instrument-disable=num-ready --instrument-disable=graph-size  --instrument-disable=api --instrument-disable=state"
    export LD_PRELOAD=${EXTRAE_HOME}/lib/libnanosmpitrace.so
else
    export LD_PRELOAD=${EXTRAE_HOME}/lib/libompitrace.so
fi

\${EXEC} \${EXTRA_FLAGS} -i ${input_f}
EOOF
        chmod +x \${script}
        srun ./\${script} > output 2> error
        popd &> /dev/null
    done
done
EOF
chmod +x lammps.sh
sbatch lammps.sh
