#!/bin/bash

#MODE="STRONG"
MODE="WEAK"
if [ "${MODE}" == "STRONG" ]; then
    DIR=StrongScaling
    SDIRS=$( find ${DIR} -maxdepth 1 -type d | tail -n +2 | sort )
    for mod in "OMPSS" "OMP" "KOKKOS"; do
        echo 
        for threads in 1 2 4 6 8 12 24; do
            echo "==== ${mod} threads=${threads}"
            for sdir in ${SDIRS}; do
                ncpus=$( expr "${sdir: -4}" + 0 )
                file=$( ls $( printf "${sdir}/${mod}-*-%02d.out" "${threads}" ) 2> /dev/null )
                if [ -f "${file}" ]; then
                    MSG=$( grep "Loop time of"  ${file} | cut -d" " -f 4 )
                    if [ -z "${MSG}" ]; then
                        echo "MISSINF TEXT: d=${sdir}  mod=${mod}  nthreads=${threads}"
                    else
                        echo ${MSG}
                    fi
                else
                    echo "MISSING FILE: d=${sdir}  mod=${mod}  nthreads=${threads}"
                fi
            done
            echo ""
        done
    done
fi

if [ "${MODE}" == "WEAK" ]; then echo YE
    DIR=WeakScaling
    SDIRS=$( find ${DIR} -maxdepth 1 -type d | tail -n +2 | sort )
    for mod in "OMPSS" "OMP" "KOKKOS"; do
        echo 
        for threads in 1 6 8 12; do
            echo "==== ${mod} threads=${threads}"
            for sdir in ${SDIRS}; do
                ncpus=$( expr "${sdir: -4}" + 0 )
                file=$( ls $( printf "${sdir}/${mod}-*-%02d.out" "${threads}" ) 2> /dev/null )
                if [ -f "${file}" ]; then
                    MSG=$( grep "Loop time of"  ${file} | cut -d" " -f 4 )
                    if [ -z "${MSG}" ]; then
                        echo "MISSINF TEXT: d=${sdir}  mod=${mod}  nthreads=${threads}"
                    else
                        echo ${MSG}
                    fi
                else
                    echo "MISSING FILE: d=${sdir}  mod=${mod}  nthreads=${threads}"
                fi
            done
            echo ""
        done
    done
fi
exit

for mod in "OMP" "OMPSS" "KOKKOS"; do
    FILES=$( find ${DIR} -name ${mod}-*.out )
    for f in ${FILES}; do
        grep "Loop time of" $f
        basename $f
    done
done
