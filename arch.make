CFLAGS= -fPIC -g -O3
CXXFLAGS= -fPIC -g -O3
FFLAGS= -fPIC -g -O3

ifeq ($(MPI),impi)
  GNU=NO
  CC=icc
  CXX=icpc
  FC=ifort
  NO_OMPSS=-restrict
  MCXX=imcxx
  MPICXX=mpiicpc
  MPI_PRE=I_MPI_CXX
else
  GNU=YES
  CC=gcc
  CXX=g++
  FC=gfortran
  MCXX=smpcxx
  MPICXX=mpicxx
  MPI_PRE=OMPI_CXX
endif

OMPSS=$(shell which $(MCXX) 2>&1 > /dev/null > /dev/null && echo YES || echo NO)
ifeq ($(OMPSS),NO)
      $(info module load ompss)
      $(error ================)
endif

DEFINES= -DLAMMPS_GZIP -DLAMMPS_MEMALIGN=64 -DMPICH_SKIP_MPICXX -DOMPI_SKIP_MPICXX=1 \
         -D_OMPSS -DOMPSS_NSUB_TASK=4
OMPSS_FLAGS=--ompss

