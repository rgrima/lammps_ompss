TOPDIR=$(shell pwd)
BASE_NAME=ompss

all: extrae basic

basic:
	+@make -C src TOPDIR=$(TOPDIR) DESTDIR=$(TOPDIR)/obj     EXE=$(TOPDIR)/lmp_$(BASE_NAME)     EXTRA_FLAGS=

extrae:
	+@make -C src TOPDIR=$(TOPDIR) DESTDIR=$(TOPDIR)/obj_ext EXE=$(TOPDIR)/lmp_$(BASE_NAME)_ext EXTRA_FLAGS="--instrument"

clean:
	+@make -C src TOPDIR=$(TOPDIR) DESTDIR=$(TOPDIR)/obj     EXE=$(TOPDIR)/lmp_$(BASE_NAME)     clean
	+@make -C src TOPDIR=$(TOPDIR) DESTDIR=$(TOPDIR)/obj_ext EXE=$(TOPDIR)/lmp_$(BASE_NAME)_ext clean

tar:
	@tar cvfz $(BASE_NAME).tgz  src $$( find bench/ -maxdepth 2 -type f | grep -e .sh$$ -e data. -e in. -e .xml$$ ) Makefile arch.make
