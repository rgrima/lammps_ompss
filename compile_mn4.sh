#!/bin/bash
#SBATCH --job-name=compile
#SBATCH --workdir=.
#SBATCH --output=.out
#SBATCH --error=.err
#SBATCH --cpus-per-task=48
#SBATCH --ntasks=1
#SBATCH --time=00:10:00
# #SBATCH --qos=debug

#module swap intel intel/2018.3
#module swap impi impi/2018.3

#LIST_MOD=$( module tablelist 2>&1 | grep \" | cut -d\" -f 2 )
#echo ${LIST_MOD} | grep -w intel > /dev/null && module swap intel gcc/7.1.0
#echo ${LIST_MOD} | grep -w impi  > /dev/null && module swap impi openmpi/3.1.1
#echo ${LIST_MOD} | grep -w intel > /dev/null && module swap intel gcc/7.2.0
#echo ${LIST_MOD} | grep -w impi  > /dev/null && module swap impi openmpi
#echo ${LIST_MOD} | grep -w ompss > /dev/null || module load ompss


GNU=NO
LISTMOD=$(echo $LOADEDMODULES | tr ":" "\n")
if [ "${GNU}" == "YES" ]; then
    MOD=$( echo "${LISTMOD}" | grep -w intel ) && module swap ${MOD} gcc/7.1.0
    MOD=$( echo "${LISTMOD}" | grep -w impi ) && module swap ${MOD} openmpi
    MOD=$( echo "${LISTMOD}" | grep -w ompss ) || module load ompss
    export OMPI_CC=smpcc
    export OMPI_CXX=smpcxx
    export OMPI_FC=smpfc
else
    MOD=$( echo "${LISTMOD}" | grep -w gcc ) && module swap ${MOD} intel
    MOD=$( echo "${LISTMOD}" | grep -w openmpi ) && module swap ${MOD} impi
    MOD=$( echo "${LISTMOD}" | grep -w ompss ) || module load ompss
    export I_MPI_CC=imcc
    export I_MPI_CXX=imcxx
    export I_MPI_FC=imf95
fi

make -j 48 basic &
make -j 48 extrae 
wait

